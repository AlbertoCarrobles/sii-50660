#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<fcntl.h>

int main(int argc,char*argv[]){
	mkfifo("/tmp/loggerfifo",0777);	
	int fd=open("/tmp/loggerfifo",O_RDONLY);
	int salir=0;
	int aux;
	while(salir==0)
	{
		char buff[200];
		aux=read(fd,buff,sizeof(buff));
		printf("%s\n",buff);
		if((buff[0]=='-')||(aux==-1)) //para asegurarnos que no hay fallos en el read
		{
		printf("---Tenis cerrado, CERRANDO logger...--- \n");
		salir=1; // al darse la condición se sale del bucle
		}
	}
	close(fd);
	unlink("tmp/loggerfifo");
	return 0;
}
